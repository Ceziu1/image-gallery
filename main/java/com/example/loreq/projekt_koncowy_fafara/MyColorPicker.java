package com.example.loreq.projekt_koncowy_fafara;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewManager;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by Loreq on 2016-11-30.
 */
public class MyColorPicker extends View {
    RelativeLayout box;
    ImageView accept;
    ImageView circle;
    TextView title;
    Integer kolor;

    public RelativeLayout getBox() {
        return box;
    }
    public Integer getKolor(){
        return kolor;
    }

    public MyColorPicker(Context context) {
        super(context);
        //główny pojemnik na kolornik
        box = new RelativeLayout(context);
        // Defining the RelativeLayout layout parameters.
        // In this case I want to fill its parent
        RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT);
        box.setLayoutParams(rlp);
//        box.setBackgroundColor(0x547fc8);
        box.setBackgroundColor(0xFF547FC8);

        //nasz ukochany obrazek będący główną częścią pickera
        circle = new ImageView(context);
        circle.setImageResource(R.drawable.color);
        circle.setLayoutParams(new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT
        ));
        circle.setDrawingCacheEnabled(true);

        circle.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Bitmap bmp = v.getDrawingCache();
                kolor = bmp.getPixel((int)event.getX(), (int)event.getY());
                return true;
            }
        });
        box.addView(circle);
        accept = new ImageView(context);
        accept.setImageResource(R.drawable.ok);
        accept.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ViewManager)box.getParent()).removeView(box);
            }
        });
        box.addView(accept);
    }
}
