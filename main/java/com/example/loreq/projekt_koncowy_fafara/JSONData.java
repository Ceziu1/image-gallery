package com.example.loreq.projekt_koncowy_fafara;

/**
 * Created by 4id1 on 2016-12-08.
 */
public class JSONData {
    public String imageName, imageSaveTime, imageSize;

    public JSONData(String imageName, String imageSaveTime, String imageSize) {
        this.imageName = imageName;
        this.imageSaveTime = imageSaveTime;
        this.imageSize = imageSize;
    }
}
