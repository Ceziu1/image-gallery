package com.example.loreq.projekt_koncowy_fafara;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

import activities.AlbumSelector;
import activities.Camera_activity;
import activities.Gallery_view;

/**
 * Created by 4id1 on 2016-09-22.
 */
public class myArrayAdapter extends ArrayAdapter {

    private ArrayList<String> array;
    private Context _context;

    public myArrayAdapter(Context context, int resource, ArrayList<String> objects) {
        super(context, resource, objects);
        array = objects;
        _context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.album_cell, null);
        Log.e("qq", String.valueOf(position));
        final TextView tv1 = (TextView) convertView.findViewById(R.id.folderName);
        tv1.setText(array.get(position));
        final int index= position;

        ImageView iv1 = (ImageView) convertView.findViewById(R.id.albumRemove);
        iv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert = new AlertDialog.Builder(_context);
                alert.setTitle("Usuwanie");
                alert.setMessage("Na pewno usunąć katalog " + array.get(index) + "?");
                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "/fafara");
                    File dir = new File(file, array.get(index));
                    for (File temp : dir.listFiles()){
                        temp.delete();
                    }
                    dir.delete();
                    ((AlbumSelector) _context).listDir();
                    }
                });
                alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    return;
                    }
                });
                alert.show();
            }
        });
        return convertView;
    }
}
