package com.example.loreq.projekt_koncowy_fafara;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;

/**
 * Created by 4id1 on 2016-10-13.
 */
public class Miniature extends ImageView{
    private Bitmap bitMap;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    private int index= 0;

    public Miniature(Context context, Bitmap b, int width) {
        super(context);
//        Matrix matrix = new Matrix();
//        matrix.postRotate(90);
//        this.bitMap = Bitmap.createScaledBitmap(b, width, width, false);
//        Bitmap rotatedBitmap = Bitmap.createBitmap(this.bitMap , 0, 0, this.bitMap.getWidth(), this.bitMap.getHeight(), matrix, true);
        this.bitMap = Bitmap.createScaledBitmap(b, width, width, false);
        setImageBitmap(Imaging.returnRotatedBitmap(this.bitMap));
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, width);
        setLayoutParams(layoutParams);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(10.0f);
        paint.setColor(Color.argb(150, 0, 255, 255));
        canvas.drawRect(0,0,100, 100, paint);
    }
}
