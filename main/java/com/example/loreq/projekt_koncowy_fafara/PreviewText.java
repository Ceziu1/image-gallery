package com.example.loreq.projekt_koncowy_fafara;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.view.View;

/**
 * Created by Loreq on 2016-11-12.
 */
public class PreviewText extends View {

    private Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private String _txt;
    private Integer cl1, cl2;

    public PreviewText(Context context, Typeface tf, String txt,Integer c1, Integer c2) {
        super(context);
        if(c1==null || c2==null){
            this.cl1 = Color.BLACK;
            this.cl2 = Color.BLACK;
        }
        else{
            this.cl1 = c1;
            this.cl2 = c2;
        }
        this._txt = txt;
        paint.reset();            // czyszczenie
        paint.setAntiAlias(true);    // wygładzanie
        paint.setTextSize(100);        // wielkość fonta
        paint.setTypeface(tf);  // czcionka
    }

    public Rect getRect(){
        Rect rect = new Rect();
        paint.getTextBounds(this._txt, 0, this._txt.length(), rect);
        return rect;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(cl1);
        canvas.drawText(this._txt, 0, 100, paint);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(5);
        paint.setColor(cl2);
        canvas.drawText(this._txt, 0, 100, paint);
    }
}
