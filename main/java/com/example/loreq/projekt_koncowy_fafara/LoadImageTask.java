package com.example.loreq.projekt_koncowy_fafara;

import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import activities.MainActivity;

/**
 * Created by 4id1 on 2016-12-08.
 */
public class LoadImageTask extends AsyncTask<String, Void, String> {
    public static ArrayList<Drawable> getLoadedImage() {
        return loadedImage;
    }

    private static ArrayList<Drawable> loadedImage= new ArrayList<>();
    private String in;
    private ProgressDialog pp;

    public LoadImageTask(String imageName) {
        this.in = imageName;
    }

    public Drawable LoadImageFromWeb(String url) throws IOException {
        InputStream inputStream = (InputStream) new URL(url).getContent();
        return Drawable.createFromStream(inputStream, this.in);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pp = ProgressDialog.show(MainActivity.c, "Ładownie", "Trwa ladowanie zdjec", true);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        ImageView imageView = (ImageView) ((MainActivity)MainActivity.c).findViewById(R.id.herokuMini);
        TextView textView = (TextView) ((MainActivity)MainActivity.c).findViewById(R.id.herokuDesc);
        TextView textView1 = (TextView) ((MainActivity)MainActivity.c).findViewById(R.id.herokuSize);
        TextView textView2 = (TextView) ((MainActivity)MainActivity.c).findViewById(R.id.herokuDate);
        imageView.setImageDrawable(LoadImageTask.getLoadedImage().get(0));
        textView.setText(DownloadPhoto.jsonDataArray.get(0).imageName);
        textView1.setText(DownloadPhoto.jsonDataArray.get(0).imageSize + " KB");
        textView2.setText(DownloadPhoto.jsonDataArray.get(0).imageSaveTime);
        pp.dismiss();
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            loadedImage.add(LoadImageFromWeb(GlobalVariable.serverURL + this.in));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
