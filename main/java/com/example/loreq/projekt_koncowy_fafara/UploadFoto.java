package com.example.loreq.projekt_koncowy_fafara;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import activities.GalleryManager;

/**
 * Created by 4id1 on 2016-12-01.
 */
public class UploadFoto extends AsyncTask<String, Void, String> {
    public static ProgressDialog pDialog;
    private String result = "";

    @Override
    protected String doInBackground(String... strings) {
         HttpPost httpPost = new HttpPost(GlobalVariable.serverURL);
         httpPost.setEntity(new ByteArrayEntity(GalleryManager.bFile));
         DefaultHttpClient httpClient = new DefaultHttpClient();
         HttpResponse httpResponse = null;
         try {
             httpResponse = httpClient.execute(httpPost);
             result = EntityUtils.toString(httpResponse.getEntity(), HTTP.UTF_8);
         } catch (IOException e) {
             e.printStackTrace();
         }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pDialog.show();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        pDialog.dismiss();
        Log.e("qq", result);
    }
}
