package com.example.loreq.projekt_koncowy_fafara;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

/**
 * Created by 4id1 on 2016-10-13.
 */
public class Imaging {

    public static byte[] datas ;

    public static Bitmap returnBitmap(byte[] data){
        return BitmapFactory.decodeByteArray(data, 0, data.length);
    }

    public static Bitmap returnRotatedBitmap(Bitmap b){
        Matrix matrix = new Matrix();
        matrix.postRotate(90);
        return Bitmap.createBitmap(b , 0, 0, b.getWidth(), b.getHeight(), matrix, true);
    }

}
