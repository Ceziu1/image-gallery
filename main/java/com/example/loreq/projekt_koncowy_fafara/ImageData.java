package com.example.loreq.projekt_koncowy_fafara;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by 4id1 on 2016-10-27.
 */
public class ImageData implements Serializable {
    private float x;

    private float y;
    private float w;
    private float h;

    public ImageData(float x, float y, float w, float h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getW() {
        return w;
    }

    public float getH() {
        return h;
    }
}
