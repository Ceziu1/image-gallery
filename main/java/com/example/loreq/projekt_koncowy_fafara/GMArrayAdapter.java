package com.example.loreq.projekt_koncowy_fafara;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import activities.GalleryManager;
import activities.Gallery_view;
import activities.Letters;

/**
 * Created by 4id1 on 2016-11-10.
 */
public class GMArrayAdapter extends ArrayAdapter {

    private ArrayList<String> arrayObjects;
    private Context _context;
    private int _resource;

    public GMArrayAdapter(Context context, int resource,ArrayList<String> objects) {
        super(context, resource, objects);
        arrayObjects = objects;
        _context = context;
        _resource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(_resource, null);
        TextView tv1 = (TextView) convertView.findViewById(R.id.folderName);
        ImageView iv1 = (ImageView) convertView.findViewById(R.id.cellImage);
        tv1.setText(arrayObjects.get(position));
        switch (position){
            case 0:
                iv1.setImageDrawable(_context.getResources().getDrawable(R.drawable.camera));
                final View finalConvertView = convertView;
                iv1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(_context, Letters.class);
                        ((GalleryManager) _context).startActivityForResult(intent,100);
                    }
                });
                break;
            case 1:
                iv1.setImageDrawable(_context.getResources().getDrawable(R.drawable.dashboard));
                break;
            case 2:
                iv1.setImageDrawable(_context.getResources().getDrawable(R.drawable.flash));
                break;
            case 3:
                iv1.setImageDrawable(_context.getResources().getDrawable(R.drawable.explore));
                break;
            default:
                break;
        }
        return convertView;
    }
}
