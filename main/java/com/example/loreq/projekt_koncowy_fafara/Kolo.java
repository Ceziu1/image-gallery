package com.example.loreq.projekt_koncowy_fafara;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

/**
 * Created by 4id1 on 2016-10-13.
 */
public class Kolo extends View{
    private float x, y, r;

    public Kolo(Context context, float x, float y, float r) {
        super(context);
        this.r = r;
        this.x = x;
        this.y = y;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(5.0f);
        paint.setColor(Color.argb(150, 255, 255, 255));
        canvas.drawCircle(this.x, this.y, this.r, paint);
    }
}
