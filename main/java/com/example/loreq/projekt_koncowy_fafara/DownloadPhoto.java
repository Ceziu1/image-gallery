package com.example.loreq.projekt_koncowy_fafara;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import activities.MainActivity;

/**
 * Created by 4id1 on 2016-12-08.
 */
public class DownloadPhoto extends AsyncTask<String, Void, String> {
    private JSONArray allImagesJson = new JSONArray();
    public static ArrayList<JSONData> jsonDataArray = new ArrayList<>();

    private ProgressDialog pp;

    public ArrayList<JSONData> getJsonDataArray() {
        return jsonDataArray;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pp = ProgressDialog.show(MainActivity.c, "Ładownie", "Trwa pobieranie danych", true);
    }

    @Override
    protected String doInBackground(String... strings) {
        HttpPost httpPost = new HttpPost(GlobalVariable.serverURL + "mjson");
        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpResponse httpResponse = null;
        String jsonString = null;
        JSONObject jsonObj = null;
        if(jsonDataArray != null)
            jsonDataArray.clear();
        try {
            httpResponse = httpClient.execute(httpPost);
            jsonString = EntityUtils.toString(httpResponse.getEntity(), HTTP.UTF_8);
            jsonObj = new JSONObject(jsonString);
            allImagesJson = jsonObj.getJSONArray("photoArray");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < allImagesJson.length(); i++) {
            JSONObject object = null;
            try {
                object = allImagesJson.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String imageName = null;
            String imageSaveTime = null;
            String imageSize = null;
            try {
                imageName = object.getString("name");
                imageSaveTime = object.getString("date");
                imageSize = object.getString("size");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonDataArray.add(new JSONData(imageName, imageSaveTime, imageSize));
        }

        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        for (int i = 0; i < jsonDataArray.size(); i++) {
            new LoadImageTask(jsonDataArray.get(i).imageName).execute();
        }
        pp.dismiss();
    }
}
