package activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.IntegerRes;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.ListViewAutoScrollHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.example.loreq.projekt_koncowy_fafara.GMArrayAdapter;
import com.example.loreq.projekt_koncowy_fafara.GlobalVariable;
import com.example.loreq.projekt_koncowy_fafara.Imaging;
import com.example.loreq.projekt_koncowy_fafara.Networking;
import com.example.loreq.projekt_koncowy_fafara.OnSwipeTouchListener;
import com.example.loreq.projekt_koncowy_fafara.PreviewText;
import com.example.loreq.projekt_koncowy_fafara.R;
import com.example.loreq.projekt_koncowy_fafara.UploadFoto;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class GalleryManager extends AppCompatActivity {
    public static byte[] bFile;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case 100:
                Bundle extras = data.getExtras();
                String text = (String)extras.get("text");
                String fontname = (String)extras.get("fontname");
                Integer c1 = (Integer) extras.get("c1");
                Integer c2 = (Integer)extras.get("c2");
                Log.d("return", text + "   " + fontname) ;
                addText(text,fontname,c1,c2);
                break;
            default:
                break;
        }
    }

    private void addText(String text, String fontname,Integer c1, Integer c2) {
        RelativeLayout rl = (RelativeLayout)findViewById(R.id.afoto_rl) ;
        Typeface tf=Typeface.createFromAsset(getAssets(),"fonts/"+ fontname);
        final PreviewText previewText = new PreviewText(this,tf,text, c1, c2) ;
        //rl.removeAllViews();
        final Float[] posx = {null};
        final Float[] posy = { null };
        previewText.setLayoutParams(new LinearLayout.LayoutParams(
                previewText.getRect().width(),
                previewText.getRect().height()
        ));
        previewText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction())
                {
                    case MotionEvent.ACTION_MOVE:
                        previewText.setX(previewText.getX() + event.getX() - posx[0]);
                        previewText.setY(previewText.getY() + event.getY() - posy[0]);
                        break;

                    case MotionEvent.ACTION_UP:
                        previewText.setBackgroundColor(Color.TRANSPARENT);
                        break;

                    case MotionEvent.ACTION_DOWN:
                        previewText.setBackgroundColor(Color.YELLOW);
                        posx[0] = event.getX() ;
                        posy[0] = event.getY() ;
                        previewText.bringToFront();
                        break;
                }
                return true;
            }
        });
        rl.addView(previewText);
        Log.d("create","create") ;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_manager);
        getSupportActionBar().hide();

        Bundle bundle = getIntent().getExtras();
        final String file = bundle.getString("path").toString();
        GlobalVariable.ImageToSendName = file;

        ImageView iv = (ImageView) findViewById(R.id.GMImage);
        Display display = getWindowManager().getDefaultDisplay();

        File qq = new File(file);
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(qq.getAbsolutePath(),bmOptions);
        bitmap = Bitmap.createScaledBitmap(bitmap,display.getHeight(),display.getWidth(),true);
        iv.setImageBitmap(bitmap);

        iv.setOnTouchListener(new OnSwipeTouchListener(GalleryManager.this){
            @Override
            public void onSwipeRight() throws InterruptedException {
                super.onSwipeRight();

                ArrayList<String> temp = new ArrayList<>();
                temp.add("Font");
                temp.add("Option_1");
                temp.add("Send");
                temp.add("Share");
                GMArrayAdapter gma = new GMArrayAdapter(
                        GalleryManager.this,
                        R.layout.cell_view,
                        temp
                );
                ListView AlbumManager = (ListView) findViewById(R.id.GMListView);
                AlbumManager.setAdapter(gma);
                AlbumManager.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    switch(i){
                        case 0:
                            Intent intent = new Intent(GalleryManager.this, Letters.class);
                            startActivityForResult(intent, 100);
                            break;
                        case 2:
                            if(Networking.getConnection(GalleryManager.this)){
                                FileInputStream fileInputStream=null;
                                File fileToByte = new File(file);
                                bFile = new byte[(int) fileToByte.length()];
                                try {
                                    //convert file into array of bytes
                                    fileInputStream = new FileInputStream(fileToByte);
                                    fileInputStream.read(bFile);
                                    fileInputStream.close();
                                } catch (Exception e){
                                    e.printStackTrace();
                                }
                                UploadFoto.pDialog = new ProgressDialog(GalleryManager.this);
                                UploadFoto.pDialog.setMessage("komunikat");
                                UploadFoto.pDialog.setCancelable(false);
                                new UploadFoto().execute();
                            } else {
                                AlertDialog.Builder alert = new AlertDialog.Builder(GalleryManager.this);
                                alert.setTitle("WiFi");
                                alert.setMessage("Nie ma interneta");
                                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        return;
                                    }
                                });
                                alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        return;
                                    }
                                });
                                alert.show();
                            }
                            break;
                        case 3:
                            if(Networking.getConnection(GalleryManager.this)){
                                Intent share = new Intent(Intent.ACTION_SEND);
                                share.setType("image/jpeg"); //typ danych który chcemy współdzielić
                                DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                                Date date = new Date();
                                String tempFileName = "tymczasowy" + dateFormat.format(date) + ".jpg"; // dodaj bieżąca datę do nazwy pliku
                                FileInputStream fileInputStream=null;
                                File fileToByte = new File(file);
                                bFile = new byte[(int) fileToByte.length()];
                                Display display = getWindowManager().getDefaultDisplay();
                                try {
                                    //convert file into array of bytes
                                    fileInputStream = new FileInputStream(fileToByte);
                                    fileInputStream.read(bFile);
                                    fileInputStream.close();
                                    FileOutputStream fs = new FileOutputStream(Environment.getExternalStorageDirectory() + "/" + tempFileName);
                                    Bitmap bitmapd = Imaging.returnBitmap(bFile);
                                    bitmapd.compress(Bitmap.CompressFormat.JPEG, 50, fs);
                                    fs.close();
                                }catch(Exception e){
                                    e.printStackTrace();
                                }
                                share.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///" + String.valueOf(Environment.getExternalStorageDirectory()) + "/" + tempFileName)); //pobierz plik i podziel się nim:
                                startActivity(Intent.createChooser(share, "Podziel się plikiem!")); //pokazanie okna share
                            } else{
                                AlertDialog.Builder alert = new AlertDialog.Builder(GalleryManager.this);
                                alert.setTitle("WiFi");
                                alert.setMessage("Nie ma interneta");
                                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        return;
                                    }
                                });
                                alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        return;
                                    }
                                });
                                alert.show();
                            }
                        default:
                            break;
                    }
                    }
                });

                DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });
    }
}
