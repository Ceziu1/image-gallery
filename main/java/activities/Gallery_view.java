package activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.loreq.projekt_koncowy_fafara.R;

import java.io.File;

public class Gallery_view extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.images_boxes);
        getSupportActionBar().hide();

        Bundle bundle = getIntent().getExtras();
        String files = bundle.getString("path").toString();
        if(files!="")
            showPhotos(files);
    }
    public void showPhotos(String files){
        File file = new File(files);
        File[] objects = file.listFiles();
        int zm = 0 ;
        for(int i = 0 ; i<objects.length ; i++){
            if(objects[i].isFile()){
                final ImageView imageView = new ImageView(this) ;
                LinearLayout.LayoutParams lparams;
                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int width = size.x;
                int height = size.y;
                if(zm==0 || zm==4) {
                    lparams = new LinearLayout.LayoutParams(width / 2 - 2, 300);
                    zm++ ;
                }
                else if(zm==1 || zm==2 || zm==3){
                    lparams = new LinearLayout.LayoutParams(width/2 - 2,200) ;
                    zm++ ;
                }
                else{
                    lparams = new LinearLayout.LayoutParams(width/2 - 2,200) ;
                    zm=0 ;
                }
                // lparams.setMargins(0,0,0,2);
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

                //zmiana wielkości ImageView wg tych parametrów

                imageView.setLayoutParams(lparams);
                final String imagePath = objects[i].getPath() ;
                Bitmap bmp = betterImageDecode(imagePath);    // funkcja decodeImage opisana jest ponizej
                imageView.setImageBitmap(bmp);        // wstawienie bitmapy do ImageView
                LinearLayout hsc ;

                //hsc.addView(imageView);
                if(zm==1 || zm==4){
                    hsc = (LinearLayout) findViewById(R.id.llv1) ;
                }
                else if(zm==2 || zm==3 || zm==5) {
                    hsc = (LinearLayout) findViewById(R.id.llv2) ;
                }
                else{
                    hsc = (LinearLayout) findViewById(R.id.llv1) ;
                }
                hsc.addView(imageView);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Gallery_view.this, GalleryManager.class);
                        intent.putExtra("path", imagePath);
                        startActivity(intent);
                    }
                });
            }

        }

    }
    private Bitmap betterImageDecode(String filePath) {
        Bitmap myBitmap;
        BitmapFactory.Options options = new BitmapFactory.Options();    //opcje przekształcania bitmapy
        options.inSampleSize = 4; // zmniejszenie jakości bitmapy 4x
        myBitmap = BitmapFactory.decodeFile(filePath, options);
        return myBitmap;
    }
}
