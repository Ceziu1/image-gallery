package activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.example.loreq.projekt_koncowy_fafara.DownloadPhoto;
import com.example.loreq.projekt_koncowy_fafara.GlobalVariable;
import com.example.loreq.projekt_koncowy_fafara.LoadImageTask;
import com.example.loreq.projekt_koncowy_fafara.R;

public class ImagesFromWeb extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_images_from_web);
        getSupportActionBar().hide();

        for (int i= 0; i< LoadImageTask.getLoadedImage().size(); i++){
            final ImageView iv = new ImageView(this);
            iv.setImageDrawable(LoadImageTask.getLoadedImage().get(i));
            Display display = getWindowManager().getDefaultDisplay();
            int height = display.getHeight();  // deprecated
            iv.setMinimumHeight(height);
            final int finalI = i;
            iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse(GlobalVariable.serverURL + DownloadPhoto.jsonDataArray.get(finalI).imageName.split("-")[0] + ".png"));
                    startActivity(intent);
                }
            });
            LinearLayout ll = (LinearLayout) findViewById(R.id.ImagesFromWeb);
            ll.addView(iv);
        }
    }
}
