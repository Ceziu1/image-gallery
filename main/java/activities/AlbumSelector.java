package activities;

import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.loreq.projekt_koncowy_fafara.GlobalVariable;
import com.example.loreq.projekt_koncowy_fafara.R;
import com.example.loreq.projekt_koncowy_fafara.myArrayAdapter;
//import android.widget.ArrayAdapter;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.IOError;
import java.util.ArrayList;
import java.util.Arrays;

public class AlbumSelector extends AppCompatActivity {

    private File rootDir;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_selector);
        getSupportActionBar().hide();
        rootDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "/fafara");
        listDir();
    }

    public void goToCamera(View view){
        ImageView goTC = (ImageView) findViewById(R.id.goToCamera);
        final EditText folderName = (EditText) findViewById(R.id.folder_name);
        Intent intent = new Intent(AlbumSelector.this, Camera_activity.class);
        GlobalVariable.cameraFolderSave = String.valueOf(folderName.getText());
        intent.putExtra("path", String.valueOf(folderName.getText()));
        startActivity(intent);
    }

    public void listDir(){
        File[] dirList = rootDir.listFiles();     //tablica plików
        Arrays.sort(dirList);                     // sortowanie plików wg nazwy
        ArrayList<String> lista = new ArrayList<String>();
        ArrayList<String> temp = new ArrayList<>();
        for (int i= 0; i< dirList.length; i++){
            lista.add(dirList[i].getPath());
            temp.add(lista.get(i).split("/")[lista.get(i).split("/").length-1]);
        }
        myArrayAdapter adapter = new myArrayAdapter(
                AlbumSelector.this,
                R.layout.album_cell,
                temp
        );

        GridView AlbumManager = (GridView) findViewById(R.id.amGridView);
        AlbumManager.setAdapter(adapter);
    }

}
