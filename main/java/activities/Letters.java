package activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.loreq.projekt_koncowy_fafara.MyColorPicker;
import com.example.loreq.projekt_koncowy_fafara.PreviewText;
import com.example.loreq.projekt_koncowy_fafara.R;

import java.io.IOException;

public class Letters extends AppCompatActivity {
    private MyColorPicker colorPicker1;
    private MyColorPicker colorPicker2;
    private PreviewText previewText;
    private Integer c1, c2;
    private Integer pCols;

    Context cn ;
    Typeface tff ;
    String text = "" ;
    String fontname="" ;
    String strokeColor = "";
    String fillColor = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_letters);
        getSupportActionBar().hide();
        cn = this ;
        c1 = Color.BLACK;
        c2 = Color.BLACK;
        pCols = 0;
        showFonts();

        ImageButton ib1 = (ImageButton) findViewById(R.id.imageButton5);
        ib1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnFont(v);
            }
        });
        ImageButton fill = (ImageButton) findViewById(R.id.fillColor);
        fill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorPicker2 = new MyColorPicker(Letters.this);
                RelativeLayout picker = colorPicker2.getBox();
                RelativeLayout lettersLayout = (RelativeLayout) findViewById(R.id.letterActivityContext);
                lettersLayout.addView(picker);
                picker.bringToFront();
                pCols = 2;
            }
        });
        ImageButton stroke = (ImageButton) findViewById(R.id.strokeColor);
        stroke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorPicker1 = new MyColorPicker(Letters.this);
                RelativeLayout picker = colorPicker1.getBox();
                RelativeLayout lettersLayout = (RelativeLayout) findViewById(R.id.letterActivityContext);
                lettersLayout.addView(picker);
                picker.bringToFront();
                pCols = 1;
            }
        });

        TextWatcher textWatcher = new TextWatcher(){

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                EditText editText = (EditText)findViewById(R.id.la_et) ;
                RelativeLayout rl = (RelativeLayout)findViewById(R.id.la_topFont);
                text = editText.getText().toString() ;
                switch(pCols) {
                    case 0:
                        break;
                    case 1:
                        c1 = colorPicker1.getKolor();
                        break;
                    case 2:
                        c2 = colorPicker2.getKolor();
                        break;
                }
                previewText = new PreviewText(cn,tff, text, c1, c2) ;
                rl.removeAllViews();
                rl.addView(previewText);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        EditText editText = (EditText)findViewById(R.id.la_et) ;
        editText.addTextChangedListener(textWatcher );
    }

    private void showFonts() {
        AssetManager assetManager = getAssets();
        try {
            String[] lista = assetManager.list("fonts");
            for(int i = 0 ; i < lista.length ; i++) {
//                Typeface tf = Typeface.createFromAsset(getAssets(),"fonts/" + lista[2]);
                if(fontname=="") {
                    fontname = lista[i];
                    tff=Typeface.createFromAsset(getAssets(),"fonts/"+ lista[i]);
                }
                Log.d("font name", lista[i]) ;
                final String tx = lista[i] ;
                TextView textView = new TextView(this);
                textView.setText(lista[i]);
                textView.setTextSize(30);
                final Typeface tf=Typeface.createFromAsset(getAssets(),"fonts/"+ lista[i]);
                textView.setTypeface(tf);
                LinearLayout scrollView = (LinearLayout)findViewById(R.id.la_ll) ;
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        fontname = tx ;
                        tff = tf ;
                        RelativeLayout rl = (RelativeLayout)findViewById(R.id.la_topFont) ;
                        if(text=="")
                            previewText = new PreviewText(cn, tf, "Jogi Babu",null,null);
                        else
                            previewText = new PreviewText(cn,tf, text,null,null) ;
                        rl.removeAllViews();
                        rl.addView(previewText);
                        Log.d("create","create") ;
                    }
                });
                scrollView.addView(textView);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void returnFont(View view) {
        Intent intent = new Intent();
        intent.putExtra("text", text);
        intent.putExtra("fontname", fontname);
        intent.putExtra("c1", c1);
        intent.putExtra("c2", c2);
        setResult(100, intent);
        finish();
    }

}
