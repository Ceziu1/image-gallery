package activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.loreq.projekt_koncowy_fafara.ImageData;
import com.example.loreq.projekt_koncowy_fafara.Imaging;
import com.example.loreq.projekt_koncowy_fafara.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.function.Function;

public class CollageActivity extends AppCompatActivity {

    private int ivID= -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collage);
        getSupportActionBar().hide();
        ArrayList<ImageData> list = (ArrayList<ImageData>) getIntent().getExtras().getSerializable("collage");
        createCollage(list);

        findViewById(R.id.rotateCollage).setOnClickListener(funkcja);
        findViewById(R.id.flipCollage).setOnClickListener(funkcja);
        findViewById(R.id.screenshotCollage).setOnClickListener(funkcja);
    }

    private View.OnClickListener funkcja = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.screenshotCollage:
                    findViewById(R.id.bringToFront).setVisibility(View.GONE);
                    FrameLayout temp = (FrameLayout) findViewById(R.id.collageLayout);
                    temp.setDrawingCacheEnabled(true);
                    Bitmap b = temp.getDrawingCache(true);
                    File rootDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "/fafara");
                    File album = new File(rootDir, "kolaże");
                    if (rootDir.exists())
                        album.mkdir();
                    else
                        album.mkdirs();
                    try {
                        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                        Date date = new Date();
                        FileOutputStream fs = new FileOutputStream(album.getPath() + "/" + dateFormat.format(date) + ".jpg");
                        b.compress(Bitmap.CompressFormat.JPEG, 50, fs);
                        fs.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    findViewById(R.id.bringToFront).setVisibility(View.VISIBLE);
                    break;
                case R.id.rotateCollage:
                    if (ivID == -1) {
                        return;
                    } else {
                        Matrix matrix = new Matrix();
                        matrix.postRotate(90);
                        ImageView temp1 = (ImageView) findViewById(ivID);
                        Bitmap oryginal = ((BitmapDrawable) temp1.getDrawable()).getBitmap();
                        Bitmap rotated = Bitmap.createBitmap(oryginal, 0, 0, oryginal.getWidth(), oryginal.getHeight(), matrix, true);
                        temp1.setImageBitmap(rotated);
                    }
                    break;
                case R.id.flipCollage:
                    Matrix matrix = new Matrix();
                    matrix.postScale(-1.0f, 1.0f);
                    ImageView temp1 = (ImageView) findViewById(ivID);
                    Bitmap oryginal = ((BitmapDrawable) temp1.getDrawable()).getBitmap();
                    Bitmap rotated = Bitmap.createBitmap(oryginal, 0, 0, oryginal.getWidth(), oryginal.getHeight(), matrix, true);
                    temp1.setImageBitmap(rotated);
                    break;
            }
        }
    };

    protected void createCollage(ArrayList<ImageData> list){
        for (int i= 0; i< list.size(); i++){
            final ImageView imageView = new ImageView(this) ;
            LinearLayout.LayoutParams lparams;
            imageView.setX(list.get(i).getY());
            imageView.setY(list.get(i).getX());
            imageView.setId(i);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setLayoutParams(new LinearLayout.LayoutParams((int) list.get(i).getW(), (int) list.get(i).getH()));
            Random rnd = new Random();
            int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
            imageView.setBackgroundColor(color);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ivID= imageView.getId();
                }
            });
            imageView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(CollageActivity.this);
                    alert.setTitle("Collage");
                    final String[] opcje = {"Galeria", "Aparat systemowy", "Aparat Aplikacji"};
                    alert.setItems(opcje, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            ivID= imageView.getId();
                            switch (which){
                                case 0:
                                    Intent intent = new Intent(Intent.ACTION_PICK);
                                    intent.setType("image/*");
                                    startActivityForResult(intent, 100);
                                    break;
                                case 1:
                                    intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    if (intent.resolveActivity(getPackageManager()) != null) {
                                        startActivityForResult(intent, 200);
                                    }
                                    break;
                                case 2:
                                    Intent intent3 = new Intent(CollageActivity.this, Camera_activity.class);
                                    startActivityForResult(intent3, 300); // 200 - jw
                                default:
                                    break;
                            }
                        }
                    });
                    alert.show();
                    return false;
                }
            });
            FrameLayout temp = (FrameLayout) findViewById(R.id.collageLayout);
            temp.addView(imageView);
        }
        findViewById(R.id.bringToFront).bringToFront();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case 100:
                Uri imgData = data.getData();
                InputStream stream = null;
                try {
                    stream = getContentResolver().openInputStream(imgData);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                Bitmap b = BitmapFactory.decodeStream(stream);
                ImageView temp = (ImageView) findViewById(ivID);
                temp.setImageBitmap(b);
                break;
            case 200:
                Bundle extras = data.getExtras();
                b = (Bitmap) extras.get("data");
                temp = (ImageView) findViewById(ivID);
                temp.setImageBitmap(b);
                break;
            case 300:
                byte[] xdata = Imaging.datas;
                temp = (ImageView) findViewById(ivID);
                temp.setImageBitmap(Imaging.returnRotatedBitmap(Imaging.returnBitmap(xdata)));
                break;
            default:
                break;
        }
    }
}
