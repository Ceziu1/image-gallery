package activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;

import com.example.loreq.projekt_koncowy_fafara.ImageData;
import com.example.loreq.projekt_koncowy_fafara.R;

import java.io.Serializable;
import java.util.ArrayList;

public class ChooseCollageActivity extends AppCompatActivity{
    private ArrayList<ImageData> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_collage);
        getSupportActionBar().hide();

        ImageView col1 = (ImageView) findViewById(R.id.collage_one);
        ImageView col2 = (ImageView) findViewById(R.id.collage_two);
        ImageView col3 = (ImageView) findViewById(R.id.collage_three);
        final Display display = getWindowManager().getDefaultDisplay();

        col1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                list.clear();
                list.add(new ImageData(0,0,display.getWidth()/2,display.getHeight()));
                list.add(new ImageData(0,display.getWidth()/2,display.getWidth()/2,display.getHeight()));
                Intent intent;
                intent = new Intent(ChooseCollageActivity.this, CollageActivity.class);
                intent.putExtra("collage", list);
                startActivity(intent);
            }
        });

        col2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                list.clear();
                list.add(new ImageData(0,0,display.getWidth(),display.getHeight()/3));
                list.add(new ImageData(display.getHeight()/3,0,display.getWidth()/2,display.getHeight()/3));
                list.add(new ImageData(display.getHeight()/3,display.getWidth()/2,display.getWidth()/2,display.getHeight()/3));
                list.add(new ImageData(2*(display.getHeight()/3),0,display.getWidth(),display.getHeight()/3));
                Intent intent;
                intent = new Intent(ChooseCollageActivity.this, CollageActivity.class);
                intent.putExtra("collage", list);
                startActivity(intent);
            }
        });

        col3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                list.clear();
                list.add(new ImageData(0,0,display.getWidth()/5,display.getHeight()/3));
                list.add(new ImageData(0,display.getWidth()/5,2*(display.getWidth()/5),display.getHeight()/3));
                list.add(new ImageData(0,3*(display.getWidth()/5),2*(display.getWidth()/5),2*(display.getHeight()/3)));
                list.add(new ImageData(display.getHeight()/3,0,3*(display.getWidth()/5),display.getHeight()/3));
                list.add(new ImageData(2*(display.getHeight()/3),0,2*(display.getWidth()/5),display.getHeight()/3));
                list.add(new ImageData(2*(display.getHeight()/3),2*(display.getWidth()/5),3*(display.getWidth()/5),display.getHeight()/3));
                Intent intent;
                intent = new Intent(ChooseCollageActivity.this, CollageActivity.class);
                intent.putExtra("collage", list);
                startActivity(intent);
            }
        });
    }
}
