package activities;

import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;

import com.example.loreq.projekt_koncowy_fafara.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class File_manager extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_manager);
        getSupportActionBar().hide();

        File rootDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "/fafara");
        File[] dirList = rootDir.listFiles();     //tablica plików
        Arrays.sort(dirList);                     // sortowanie plików wg nazwy
        /////////////////
        ArrayList<String> lista = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();

        for (int i= 0; i< dirList.length; i++){
            lista.add(dirList[i].getPath());
            temp.add(lista.get(i).split("/")[lista.get(i).split("/").length-1]);
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                File_manager.this,     // Context
                R.layout.cell_view,     // nazwa pliku xml naszej komórki
                R.id.folderName,         // id pola txt w komórce
                temp );         // tablica przechowująca dane

        GridView fileManager = (GridView) findViewById(R.id.fmGridView);
        fileManager.setAdapter(adapter);

        fileManager.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e("qq", "index= " + parent.getItemAtPosition(position));
                Intent intent = new Intent(File_manager.this, Gallery_view.class);
                File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                File f  = new File(file.getPath() + "/fafara/" + parent.getItemAtPosition(position));
                intent.putExtra("path", f.getPath());
                startActivity(intent);
            }
        });
    }
}
