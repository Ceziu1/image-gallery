package activities;

import android.animation.ObjectAnimator;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.hardware.Camera;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.loreq.projekt_koncowy_fafara.CameraPreview;
import com.example.loreq.projekt_koncowy_fafara.GlobalVariable;
import com.example.loreq.projekt_koncowy_fafara.Imaging;
import com.example.loreq.projekt_koncowy_fafara.Kolo;
import com.example.loreq.projekt_koncowy_fafara.Miniature;
import com.example.loreq.projekt_koncowy_fafara.OnSwipeTouchListener;
import com.example.loreq.projekt_koncowy_fafara.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOError;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Filter;

public class Camera_activity extends AppCompatActivity {

    private Camera camera;
    private int cameraId = -1;
    private CameraPreview _cameraPreview;
    private Camera.Parameters camParams;
    private FrameLayout _frameLayout;
    public ArrayList<byte[]> photosToSave = new ArrayList<byte[]>() ;
    public ArrayList<Miniature> miniaturesArrayList = new ArrayList<Miniature>();
    public String photoPath;
    private FrameLayout cameraLayout;
    private OrientationEventListener orientationEventListener;
    private File rootDir;
    private float posb, posa;
    private boolean intentOfCollage = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_activity);
        getSupportActionBar().hide();
//        Toast.makeText(Camera_activity.this, GlobalVariable.cameraFolderSave, Toast.LENGTH_SHORT).show();
        try{
            Bundle bundle = getIntent().getExtras();
            final String files = bundle.getString("path").toString();
            rootDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "/fafara/" + files);
            photoPath = rootDir.getPath();
            if (!rootDir.exists())
                rootDir.mkdirs();
        } catch (Exception qq){
            intentOfCollage = true;
            Log.e("qq", String.valueOf(qq));
        }
        initCamera();
        initPreview();

        final ImageButton takePhotobutton = (ImageButton) findViewById(R.id.takePhoto);
        ImageButton acceptPhotobutton = (ImageButton) findViewById(R.id.acceptImage);
        ImageButton declinePhotobutton = (ImageButton) findViewById(R.id.declineImage);
        final ImageButton changeFilter = (ImageButton) findViewById(R.id.filter);
        final ImageButton changeExposition = (ImageButton) findViewById(R.id.exposition);
        final ImageButton changeWhiteBalance = (ImageButton) findViewById(R.id.whiteBalance);
        final ImageButton changeSize = (ImageButton) findViewById(R.id.resize);
        final LinearLayout bCam = (LinearLayout) findViewById(R.id.bottomCamera);
        final LinearLayout tCam = (LinearLayout) findViewById(R.id.topCamera);
        cameraLayout= (FrameLayout) findViewById(R.id.cameraLayout);
        final boolean[] animacjaMiniatur = {false};

        orientationEventListener = new OrientationEventListener(Camera_activity.this) {
            @Override
            public void onOrientationChanged(int i) {
                if (i< 300 && i> 180){
                    if (animacjaMiniatur[0])
                        return;
                    ObjectAnimator.ofFloat(takePhotobutton, View.ROTATION, 90).setDuration(300).start();
                    ObjectAnimator.ofFloat(changeFilter, View.ROTATION, 90).setDuration(300).start();
                    ObjectAnimator.ofFloat(changeExposition, View.ROTATION, 90).setDuration(300).start();
                    ObjectAnimator.ofFloat(changeWhiteBalance, View.ROTATION, 90).setDuration(300).start();
                    ObjectAnimator.ofFloat(changeSize, View.ROTATION, 90).setDuration(300).start();
                    for (int j = 2; j < cameraLayout.getChildCount(); j++) {
                        ObjectAnimator.ofFloat(cameraLayout.getChildAt(j), View.ROTATION, 90).setDuration(300).start();
                    }
                    animacjaMiniatur[0] = true;
                } else {
                    if (!animacjaMiniatur[0])
                        return;
                    ObjectAnimator.ofFloat(takePhotobutton, View.ROTATION, 0).setDuration(300).start();
                    ObjectAnimator.ofFloat(changeFilter, View.ROTATION, 0).setDuration(300).start();
                    ObjectAnimator.ofFloat(changeExposition, View.ROTATION, 0).setDuration(300).start();
                    ObjectAnimator.ofFloat(changeWhiteBalance, View.ROTATION, 0).setDuration(300).start();
                    ObjectAnimator.ofFloat(changeSize, View.ROTATION, 0).setDuration(300).start();
                    for (int j = 2; j < cameraLayout.getChildCount(); j++) {
                        ObjectAnimator.ofFloat(cameraLayout.getChildAt(j), View.ROTATION, 0).setDuration(300).start();
                    }
                    animacjaMiniatur[0]= false;
                }
            }
        };



        final Display display = getWindowManager().getDefaultDisplay();
        Kolo circle = new Kolo(Camera_activity.this, display.getWidth()/2, display.getHeight()/2, 200);
        cameraLayout.addView(circle);
        cameraLayout.setOnTouchListener(new OnSwipeTouchListener(Camera_activity.this) {
            @Override
            public void onSwipeTop() {
                super.onSwipeTop();
                ObjectAnimator anim = ObjectAnimator.ofFloat(bCam, View.TRANSLATION_Y, 200);
                anim.setDuration(300); //ms
                anim.start();
                anim = ObjectAnimator.ofFloat(tCam, View.TRANSLATION_Y, -200);
                anim.setDuration(300); //ms
                anim.start();
            }

            @Override
            public void onSwipeBottom() {
                super.onSwipeBottom();
                ObjectAnimator anim = ObjectAnimator.ofFloat(bCam, View.TRANSLATION_Y, 0);
                anim.setDuration(300); //ms
                anim.start();
                anim = ObjectAnimator.ofFloat(tCam, View.TRANSLATION_Y, 0);
                anim.setDuration(300); //ms
                anim.start();
            }
        });

        changeWhiteBalance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert = new AlertDialog.Builder(Camera_activity.this);
                alert.setTitle("White Balance");
                final String[] opcje = camParams.getSupportedWhiteBalance().toArray(new String[camParams.getSupportedWhiteBalance().size()]);
                alert.setItems(opcje, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        camParams.setWhiteBalance(opcje[which]);
                        camera.setParameters(camParams);
                    }
                });
                alert.show();
            }
        });
        changeFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert = new AlertDialog.Builder(Camera_activity.this);
                alert.setTitle("Filter List");
                final String[] opcje = camParams.getSupportedColorEffects().toArray(new String[camParams.getSupportedColorEffects().size()]);
                alert.setItems(opcje, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        camParams.setColorEffect(opcje[which]);
                        camera.setParameters(camParams);
                    }
                });
                alert.show();
            }
        });
        changeExposition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert = new AlertDialog.Builder(Camera_activity.this);
                alert.setTitle("Exposition");
                List<String> temp = new ArrayList<String>();
                for (int i = camParams.getMinExposureCompensation();i<= camParams.getMaxExposureCompensation(); i++) {
                    temp.add(String.valueOf(i));
                }
                final String[] opcje = temp.toArray(new String[temp.size()]);
                alert.setItems(opcje, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        camParams.setExposureCompensation(Integer.parseInt(opcje[which]));
                        camera.setParameters(camParams);
                    }
                });
                alert.show();
            }
        });
        changeSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert = new AlertDialog.Builder(Camera_activity.this);
                alert.setTitle("Resolution");
                final List<Camera.Size> temp = camParams.getSupportedPictureSizes();
                final String[] opcje = new String[temp.size()];
                for (int i= 0; i< camParams.getSupportedPictureSizes().size(); i++){
                    opcje[i] = temp.get(i).width + "x" + temp.get(i).height;
                }
                alert.setItems(opcje, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Log.e("kurwa", String.valueOf(temp.get(which).width));
                        camParams.setPictureSize(temp.get(which).width, temp.get(which).height);//
                        camera.setParameters(camParams);
                    }
                });
                alert.show();
            }
        });
        takePhotobutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (intentOfCollage){
                    findViewById(R.id.declineImage).setVisibility(View.VISIBLE);
                    findViewById(R.id.acceptImage).setVisibility(View.VISIBLE);
                    findViewById(R.id.takePhoto).setVisibility(View.GONE);
                }
                camera.takePicture(null, null, camPictureCallback);
            }
        });
        declinePhotobutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.declineImage).setVisibility(View.GONE);
                findViewById(R.id.acceptImage).setVisibility(View.GONE);
                findViewById(R.id.takePhoto).setVisibility(View.VISIBLE);
                photosToSave.clear();
                camera.startPreview();
            }
        });
        acceptPhotobutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.declineImage).setVisibility(View.GONE);
                findViewById(R.id.acceptImage).setVisibility(View.GONE);
                findViewById(R.id.takePhoto).setVisibility(View.VISIBLE);
//                camera.startPreview();
                Intent intent = new Intent();
                Imaging.datas = photosToSave.get(0);
                setResult(300, intent);   // 300 - jw
                finish();
//                try {
//                    File tempFile = File.createTempFile("pic", ".jpg", rootDir);
//                    FileOutputStream fos = new FileOutputStream(tempFile);
//                    fos.write(photosToSave.get(0));
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                photosToSave.clear();
            }
        });
    }

    private Camera.PictureCallback camPictureCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, final Camera camera) {
            final RelativeLayout picPrevLoy = (RelativeLayout) findViewById(R.id.ImagePreviewLaout);
            final ImageView picPrev = (ImageView) findViewById(R.id.ImagePreview);
            camera.startPreview();
//            photosToSave.clear();//do zakomentowania kiedyś tam
            photosToSave.add(data);
            camera.stopPreview();
            if (!intentOfCollage){
            final Miniature mini = new Miniature(Camera_activity.this, Imaging.returnBitmap(data), 100);
            mini.setIndex(cameraLayout.getChildCount()-2);
            miniaturesArrayList.add(mini);
            cameraLayout.addView(mini);
            final Display display = getWindowManager().getDefaultDisplay();
            mini.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(final View view) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(Camera_activity.this);
                    alert.setTitle("Picture Option");
                    final String[] option= new String[]{"podgląd", "usuń bieżące", "usuń wszystkie", "zapisz bieżące", "zapisz wszystkie"};
                    alert.setItems(option, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            final TextView tvprogress = (TextView)findViewById(R.id.handler) ;
                            switch (which){
                                case 0:
                                    tvprogress.setText("Podgląd...");
                                    Handler handler1 = new Handler();
                                    handler1.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            Bitmap b = Imaging.returnBitmap(photosToSave.get(mini.getIndex()));
                                            picPrev.setVisibility(View.VISIBLE);
                                            picPrev.setImageBitmap(Imaging.returnRotatedBitmap(Bitmap.createScaledBitmap(b, display.getHeight(),display.getWidth(), false)));
                                            picPrev.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    picPrev.setVisibility(View.GONE);
                                                }
                                            });
                                            tvprogress.setText("");
                                        }
                                    }, 1);
                                    break;
                                case 1:
                                    tvprogress.setText("Usuwanie...");
                                    handler1 = new Handler();
                                    handler1.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            int index = mini.getIndex();
                                            photosToSave.remove(mini.getIndex());
                                            cameraLayout.removeView(mini);
                                            miniaturesArrayList.remove(mini.getIndex());
                                            for (int j= 0; j< miniaturesArrayList.size(); j++){
                                                if (index< miniaturesArrayList.get(j).getIndex()){
                                                    miniaturesArrayList.get(j).setIndex(miniaturesArrayList.get(j).getIndex() - 1);
                                                }
                                            }
                                            for (int i = 2; i < cameraLayout.getChildCount(); i++) {
                                                float x = (float) (display.getWidth()/2 + (Math.cos(Math.toRadians((360/(cameraLayout.getChildCount()-2))*(i-2))) * 200))-50;
                                                float y = (float) (display.getHeight()/2 + (Math.sin(Math.toRadians((360/(cameraLayout.getChildCount()-2))*(i-2))) * 200))-50;
                                                cameraLayout.getChildAt(i).setX(x);
                                                cameraLayout.getChildAt(i).setY(y);
                                            }
                                            tvprogress.setText("");
                                        }
                                    }, 1);
                                    break;
                                case 2:
                                    tvprogress.setText("Usuwanie...");
                                    handler1 = new Handler();
                                    handler1.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            photosToSave.clear();
                                            miniaturesArrayList.clear();
                                            for (int i = cameraLayout.getChildCount(); i >= 2; i--) {
                                                cameraLayout.removeView(cameraLayout.getChildAt(i));
                                            }
                                            tvprogress.setText("");
                                        }
                                    }, 1);
                                    break;
                                case 3:
                                    tvprogress.setText("Zapisywanie...");
                                    handler1 = new Handler();
                                    handler1.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                                                Date date = new Date();
                                                FileOutputStream fs = new FileOutputStream(rootDir.getPath() + "/" + dateFormat.format(date) + ".jpg");
                                                Bitmap bitmapd = Imaging.returnBitmap(photosToSave.get(mini.getIndex()));
                                                bitmapd = Imaging.returnRotatedBitmap(Bitmap.createScaledBitmap(bitmapd, display.getHeight(), display.getWidth(), false));
                                                bitmapd.compress(Bitmap.CompressFormat.JPEG, 50, fs);
                                                fs.close();
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                            tvprogress.setText("");
                                        }
                                    }, 1);
                                    break;
                                case 4:
                                    tvprogress.setText("Zapisywanie...");
                                    handler1 = new Handler();
                                    handler1.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            for (int i = 0; i < photosToSave.size(); i++) {
                                                try {
                                                    DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                                                    Date date = new Date();
                                                    FileOutputStream fs = new FileOutputStream(rootDir.getPath() + "/" + dateFormat.format(date) + i + ".jpg");
                                                    Bitmap bitmapd = Imaging.returnBitmap(photosToSave.get(i));
                                                    bitmapd = Imaging.returnRotatedBitmap(Bitmap.createScaledBitmap(bitmapd, display.getHeight(), display.getWidth(), false));
                                                    bitmapd.compress(Bitmap.CompressFormat.JPEG, 50, fs);
                                                    fs.close();
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                            for (int i = cameraLayout.getChildCount(); i >= 2; i--) {
                                                cameraLayout.removeView(cameraLayout.getChildAt(i));
                                            }
                                            photosToSave.clear();
                                            miniaturesArrayList.clear();
                                            for (int i = 2; i < cameraLayout.getChildCount(); i++) {
                                                float x = (float) (display.getWidth()/2 + (Math.cos(Math.toRadians((360/(cameraLayout.getChildCount()-2))*(i-2))) * 200))-50;
                                                float y = (float) (display.getHeight()/2 + (Math.sin(Math.toRadians((360/(cameraLayout.getChildCount()-2))*(i-2))) * 200))-50;
                                                cameraLayout.getChildAt(i).setX(x);
                                                cameraLayout.getChildAt(i).setY(y);
                                            }
                                            tvprogress.setText("");
                                        }
                                    }, 1);
                                    break;
                                default:
                                    break;
                            }
                        }
                    });
                    alert.show();
                    return false;
                }
            });
//            mini.setOnTouchListener(new OnSwipeTouchListener(Camera_activity.this){
//                @Override
//                public void onSwipeRight() throws InterruptedException {
//                    super.onSwipeRight();
//                    ObjectAnimator anim = ObjectAnimator.ofFloat(mini, View.TRANSLATION_X, display.getHeight());
//                    anim.setDuration(600); //ms
//                    anim.start();
//                    photosToSave.remove(mini.getIndex());
//                    cameraLayout.removeView(cameraLayout.getChildAt(mini.getIndex() + 2));
//                    miniaturesArrayList.remove(mini.getIndex());
//                    for (int i = 2; i < cameraLayout.getChildCount(); i++) {
//                        float x = (float) (display.getWidth()/2 + (Math.cos(Math.toRadians((360/(cameraLayout.getChildCount()-2))*(i-2))) * 200))-50;
//                        float y = (float) (display.getHeight()/2 + (Math.sin(Math.toRadians((360/(cameraLayout.getChildCount()-2))*(i-2))) * 200))-50;
//                        cameraLayout.getChildAt(i).setX(x);
//                        cameraLayout.getChildAt(i).setY(y);
//                    }
//                }
//            });
            for (int i = 2; i < cameraLayout.getChildCount(); i++) {
                float x = (float) (display.getWidth()/2 + (Math.cos(Math.toRadians((360/(cameraLayout.getChildCount()-2))*(i-2))) * 200))-50;
                float y = (float) (display.getHeight()/2 + (Math.sin(Math.toRadians((360/(cameraLayout.getChildCount()-2))*(i-2))) * 200))-50;
                cameraLayout.getChildAt(i).setX(x);
                cameraLayout.getChildAt(i).setY(y);
            }
        }}
    };


    private void initPreview() {
        _cameraPreview = new CameraPreview(Camera_activity.this, camera);
        _frameLayout = (FrameLayout) findViewById(R.id.cameraLayout);
        _frameLayout.addView(_cameraPreview);
    }

    private void initCamera() {
        boolean cam = getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
        if (!cam) {
            Log.e("errorCamera", "Camera not found");
            return;
        } else {
            cameraId = getCameraId();
            if (cameraId < 0) {
            } else if (cameraId >= 0) {
                camera = Camera.open(cameraId);
                camParams = camera.getParameters();
            }
        }
    }

    private int getCameraId() {
        int cid = 0;
        int camerasCount = Camera.getNumberOfCameras(); // gdy więcej niż jedna kamera
        for (int i = 0; i < camerasCount; i++) {
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            Camera.getCameraInfo(i, cameraInfo);
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                cid = i;
            }
        }
        return cid;
    }

    @Override
    protected void onPause() {
        orientationEventListener.disable();
        if (camera != null) {
            camera.stopPreview();
            //linijka nieudokumentowana w API, bez niej jest crash przy wznawiamiu kamery
            _cameraPreview.getHolder().removeCallback(_cameraPreview);
            camera.release();
            camera = null;
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        if (orientationEventListener.canDetectOrientation()) {
            orientationEventListener.enable();
        } else {
            // Log - listener nie działa
        }
        if (camera == null) {
            initCamera();
            initPreview();
            camera.startPreview();
        }
        super.onResume();
    }
}
