package activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.loreq.projekt_koncowy_fafara.DownloadPhoto;
import com.example.loreq.projekt_koncowy_fafara.GlobalVariable;
import com.example.loreq.projekt_koncowy_fafara.LoadImageTask;
import com.example.loreq.projekt_koncowy_fafara.R;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    public static Context c;
    private int herokuMiniCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        c = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        ImageView iv1 = (ImageView)findViewById(R.id.left);
        ImageView iv2 = (ImageView)findViewById(R.id.right);
        iv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                herokuMiniCount--;
                if (herokuMiniCount == -1)
                    herokuMiniCount = LoadImageTask.getLoadedImage().size() - 1;
                ImageView imageView = (ImageView) ((MainActivity)MainActivity.c).findViewById(R.id.herokuMini);
                TextView textView = (TextView) ((MainActivity)MainActivity.c).findViewById(R.id.herokuDesc);
                TextView textView1 = (TextView) ((MainActivity)MainActivity.c).findViewById(R.id.herokuSize);
                TextView textView2 = (TextView) ((MainActivity)MainActivity.c).findViewById(R.id.herokuDate);
                imageView.setImageDrawable(LoadImageTask.getLoadedImage().get(herokuMiniCount));
                textView.setText(DownloadPhoto.jsonDataArray.get(herokuMiniCount).imageName);
                textView1.setText(DownloadPhoto.jsonDataArray.get(herokuMiniCount).imageSize + "KB");
                textView2.setText(DownloadPhoto.jsonDataArray.get(0).imageSaveTime);
            }
        });
        iv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                herokuMiniCount++;
                if (herokuMiniCount == LoadImageTask.getLoadedImage().size())
                    herokuMiniCount = 0;
                ImageView imageView = (ImageView) ((MainActivity)MainActivity.c).findViewById(R.id.herokuMini);
                TextView textView = (TextView) ((MainActivity)MainActivity.c).findViewById(R.id.herokuDesc);
                TextView textView1 = (TextView) ((MainActivity)MainActivity.c).findViewById(R.id.herokuSize);
                TextView textView2 = (TextView) ((MainActivity)MainActivity.c).findViewById(R.id.herokuDate);
                imageView.setImageDrawable(LoadImageTask.getLoadedImage().get(herokuMiniCount));
                textView.setText(DownloadPhoto.jsonDataArray.get(herokuMiniCount).imageName);
                textView1.setText(DownloadPhoto.jsonDataArray.get(herokuMiniCount).imageSize + "KB");
                textView2.setText(DownloadPhoto.jsonDataArray.get(0).imageSaveTime);
            }
        });

        File rootDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File album = new File(rootDir, "fafara");
        if(rootDir.exists())
            album.mkdir();
        else
            album.mkdirs();

        new DownloadPhoto().execute();
        ImageButton json = (ImageButton) findViewById(R.id.json);
        json.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ImagesFromWeb.class);
                startActivity(intent);
            }
        });
    }

    public void showFileManager(View view) {
        Log.e("background", String.valueOf(view.getBackground()));
        Intent intent = new Intent(MainActivity.this, File_manager.class);
        startActivity(intent);
    }

    public void showAlbumManager(View view){
        Intent intent;
        if (GlobalVariable.cameraFolderSave == ""){
            intent = new Intent(MainActivity.this, AlbumSelector.class);
        } else{
            intent = new Intent(MainActivity.this, Camera_activity.class);
            intent.putExtra("path", GlobalVariable.cameraFolderSave);
        }
        startActivity(intent);
    }

    public void showCollage(View view){
        Intent intent;
        intent = new Intent(MainActivity.this, ChooseCollageActivity.class);
        startActivity(intent);
    }
}
